# Vite + Vue 3 + Pinia + Vue-Router + Quasar + TypeScript + Leaflet

Init with vite:
npm init vite@latest

Add Quasar (https://quasar.dev/start/vite-plugin):
npm install quasar @quasar/extras
npm install -D @quasar/vite-plugin sass@1.32.12



SPA для отображения на карте локации пожаров.  Отчетная таблица под картой по данным пожарам.

![Alt text](image-2.png)

