import { createApp } from 'vue'
import App from './App.vue'
import { Quasar } from 'quasar'
import { createPinia } from 'pinia'
import router from './router'
// add global widgets
import AppLayout from './common/app-layout.vue';

const pinia = createPinia()
// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'

// Import Quasar css
import 'quasar/src/css/index.sass'
import iconSet from 'quasar/icon-set/mdi-v6';
import '@quasar/extras/mdi-v6/mdi-v6.css';
import 'leaflet'; // fixes BUG, must be PRIOR to: import {LMap} from '@vue-leaflet/vue-leaflet';
import 'leaflet/dist/leaflet.css';
// app specific styles, must be AFTER plugins
import './assets/styles.scss';


const app = createApp(App)

app.use(router);
app.use(pinia)
app.use(Quasar, {
  iconSet: iconSet,
  plugins: {}, // import Quasar plugins and add here
})

// GLOBAL component
app.component('app-layout', AppLayout);
app.mount('#app')
