import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../modules/HomePage.vue";
import Accident from "../modules/fireAccident/accident-view.vue";

const router = createRouter({
    // history: createWebHistory(import.meta.env.BASE_URL),
    history: createWebHashHistory(),
    routes: [
      {
        path: "/",
        name: "home",
        component: Home,
      },
      {
        path: "/accident",
        name: "accident",
        component: Accident,
      }
    ],
  });
  
  export default router;