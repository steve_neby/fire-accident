import { defineStore } from "pinia";
import { ref } from "vue";
import type { FireAccident } from "./accident.types";
import {Rest} from "../../../api/rest";

export const useAccidentStore = defineStore("accidents", () => {
  const accidentList = ref<FireAccident[]>([]);
  const error = ref("");
  const selectedAccident = ref<FireAccident[]>([]);
  const accidentPerPage = ref<FireAccident[]>([]);

  function readAccidentList(): Promise<FireAccident[]> {
    error.value = "";
    return Rest.read("accidents.json")
      .then((data) => (accidentList.value = data))
      .catch((err) => {
        if (err instanceof Error) {
          error.value = err.message;
        } else {
          error.value = JSON.stringify(err);
        }
      });
  }

  return {
    accidentList, 
    error, 
    selectedAccident,
    accidentPerPage,
    readAccidentList
  }
});
