export interface FireAccident {
    /* Название территории */
    name: string
    /* Центральная точка территории: lat: number, lon: number */
    coordinate: [number, number]
    /* Территория (гео-полигон), пострадавшая в результате пожара */ 
    burnedPolygon:[number, number][];
    /* Площадь, пострадавшей в результате пожара, территории  */
    burnedArea: number
    /* Категория ущерба (1-6), где 1 - почти нет разрушений, 6 - разрушения критические */
    hazardCategory: number
  }
