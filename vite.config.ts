import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'
// import { resolve } from 'path';
import { fileURLToPath, URL } from "url";


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(),  quasar({
    sassVariables: 'src/assets/scss/quasar-variables.scss'
  })],
  resolve: {
    // alias: [{ find: "@", replacement: resolve(__dirname, "./src") }],
    alias: [
      {find: '@', replacement: fileURLToPath(new URL('./src', import.meta.url))}
    ]
  }
})
